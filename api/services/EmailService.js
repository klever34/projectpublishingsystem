const SparkPost = require('sparkpost');
const uuid = require('uuid');

const client = new SparkPost(sails.config.settings.email.key);

module.exports = {
  sendVerificationEmail: async (email) => {
    const token = uuid.v4();
    const url = sails.config.settings.api.baseUrl;
    const createurl = `${url}/verify?token=${token}`;
    try {
      await client.transmissions.send({
        options: {
          sandbox: false,
        },
        content: {
          from: sails.config.settings.email.sender.email,
          subject: sails.config.settings.email.sender.name,
          html: `<html>
          <body>
            <h1>Please verify <a href='${createurl}'>your email</a></h1>
          </body>
        </html>`,
        },
        recipients: [{
          address: email
        }, ],
      });
      return true;
    } catch (err) {
      console.log("email error "+ err);
      return (err);
    }
  },

  sendEmail: async (email) => {
    try {
      await client.transmissions.send({
        options: {
          sandbox: false,
        },
        content: {
          from: sails.config.settings.email.sender.company,
          subject: sails.config.settings.email.sender.subject,
          html: '<html><body><p>Welcome to Uniben Project Portal</p></body></html>',
        },
        recipients: [
          { address: email },
        ],
      });
      return true;
    } catch (err) {
      return (err);
    }
  },

  sendNewAdminEmail: async (email, password) => {
    const url = sails.config.settings.api.baseUrl;
    try {
      await client.transmissions.send({
        options: {
          sandbox: false,
        },
        content: {
          from: sails.config.settings.email.sender.email,
          subject: 'New User Login details',
          html: `<html>
          <body>
            <h3>You have just been given a login access to Uniben Project Portal with this email ${email}.</h3>
            <h3>Please log in to <a href="${url}/admin/login">Uniben Project Portal</a> with this email and password ${password}.</h3>
          </body>
        </html>`,
        },
        recipients: [{
          address: email
        }, ],
      });
      return true;
    } catch (err) {
      console.log(err);
      return (err);
    }
  },

  sendDownloadLink: async (email, link) => {
    const url = sails.config.settings.api.baseUrl;
    try {
      await client.transmissions.send({
        options: {
          sandbox: false,
        },
        content: {
          from: sails.config.settings.email.sender.email,
          subject: 'Project Download link',
          html: `<html>
          <body>
            <h3>Dear user,</h3>
            <h3>Kindly follow this link ${link} to download the project.</h3>
            <h3>Thank you</h3>
            </body>
        </html>`,
        },
        recipients: [{
          address: email
        }, ],
      });
      return true;
    } catch (err) {
      console.log(err);
      return (err);
    }
  }

};
