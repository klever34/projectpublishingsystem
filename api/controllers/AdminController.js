/**
 * AdminController
 *
 * @description :: Server-side logic for managing admins
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    create: async (req, res) => {
        let data = req.body;
        try {
            if (Array.isArray(data)) {
                data = data.map(item => ({
                    ...item,
                }));
            }
            const admin = await Admin.create(data);
            await EmailService.sendNewAdminEmail(req.body.email, req.body.password);
            return ResponseService.json(201, res, 'Admin created successfully', admin);
        } catch (error) {
            return ResponseService.error(error, res);
        }
    },

    update: async (req, res) => {
        let adminUpdate = req.body;
        const conditions = {
            id: req.params.id,
            isDeleted: false,
        };
        try {
            if (Array.isArray(adminUpdate)) {
                adminUpdate = adminUpdate.map(item => ({
                    ...item,
                }));
            }
            const updatedAdmin = await Admin.update(conditions, adminUpdate);
            if (!updatedAdmin.length) {
                return ResponseService.json(404, res, 'Admin not found');
            }
            return ResponseService.json(201, res, 'Admin Updated Successfully', updatedAdmin);
        } catch (error) {
            return ValidationService.jsonResolveError(error, Admin, res);
        }
    },

    view: async (req, res) => {
        const conditions = {
            isDeleted: false,
            id: req.params.id,
        };
        try {
            const admin = await Admin.findOne(conditions);
            if (!admin) return ResponseService.json(404, res, 'Admin not found');
            return ResponseService.json(201, res, 'Admin retrieved successfully', admin);
        } catch (err) {
            return ValidationService.jsonResolveError(err, Admin, res);
        }
    },

    list: async (req, res) => {
        try {
            const conditions = { isDeleted: false };
            const records = await QueryService.find(Admin, req, conditions);
            return ResponseService.json(200, res, 'Admins retrieved successfully', records.data, records.meta);
        } catch (err) {
            return ValidationService.jsonResolveError(err, Admin, res);
        }
    },

    delete: async (req, res) => {
        let adminData = req.body;
        const conditions = {
            id: adminData.id,
            isDeleted: false,
        };
        try {
            if (Array.isArray(adminData)) {
                adminData = adminData.map(item => ({
                    ...item,
                }));
            }
            const deletedAdmin = await Admin.update(conditions, { isDeleted: true });
            if (!deletedAdmin.length) {
                return ResponseService.json(404, res, 'Admin not found');
            }
            return ResponseService.json(201, res, 'Admin deleted successfully');
        } catch (error) {
            return ResponseService.error(error, res);
        }
    },
};

