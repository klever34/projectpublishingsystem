/**
 * ThesisController
 *
 * @description :: Server-side logic for managing theses
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
const fs = require('fs');
const base64 = require('base64topdf');
module.exports = {
    create: async (req, res) => {
        let data = req.body;
        base64.base64Decode(`${req.body.thesis}`, `${req.body.fileName}.pdf`);
        fs.copyFile(`${req.body.fileName}.pdf`, `public/projects/${req.body.fileName}.pdf`, async (err) => {
        if (err) throw err;
            fs.unlink(`${req.body.fileName}.pdf`, async function (err) {
                if (err) throw err;
            });
            try {
                if (Array.isArray(data)) {
                    data = data.map(item => ({
                        ...item,
                    }));
                }
                req.body.mediaUrl = `http://localhost:1700/projects/${req.body.fileName}.pdf`;
                const thesis = await Thesis.create(data);
                await Student.update({ id: req.body.student }, { thesis: thesis.id });
                return ResponseService.json(201, res, {
                    message:'Project uploaded successfully!',
                  }, thesis);
            } catch (error) {
                return ResponseService.error(error, res);
            }
        });

    },

    update: async (req, res) => {
        let thesisUpdate = req.body;
        const conditions = {
            id: req.params.id,
            isDeleted: false,
        };
        try {
            if (Array.isArray(thesisUpdate)) {
                thesisUpdate = thesisUpdate.map(item => ({
                    ...item,
                }));
            }
            const updatedThesis = await Thesis.update(conditions, thesisUpdate);
            if (!updatedThesis.length) {
                return ResponseService.json(404, res, 'Thesis not found');
            }
            return ResponseService.json(201, res, 'Thesis Updated Successfully', updatedThesis);
        } catch (error) {
            return ValidationService.jsonResolveError(error, Thesis, res);
        }
    },

    view: async (req, res) => {
        const conditions = {
            isDeleted: false,
            id: req.params.id,
        };
        try {
            const thesis = await Thesis.findOne(conditions);
            if (!thesis) return ResponseService.json(404, res, 'Thesis not found');
            return ResponseService.json(201, res, 'Thesis retrieved successfully', thesis);
        } catch (err) {
            return ValidationService.jsonResolveError(err, Thesis, res);
        }
    },

    list: async (req, res) => {
        try {
            const conditions = { isDeleted: false };
            const records = await QueryService.find(Thesis, req, conditions);
            return ResponseService.json(200, res, 'Thesis retrieved successfully', records.data, records.meta);
        } catch (err) {
            return ValidationService.jsonResolveError(err, Thesis, res);
        }
    },

    delete: async (req, res) => {
        let thesisUpdated = req.body;
        const conditions = {
            id: thesisUpdated.id,
            isDeleted: false,
        };
        try {
            if (Array.isArray(thesisUpdated)) {
                thesisUpdated = thesisUpdated.map(item => ({
                    ...item,
                }));
            }
            const deletedThesis = await Thesis.update(conditions, { isDeleted: true });
            if (!deletedThesis.length) {
                return ResponseService.json(404, res, 'Thesis not found');
            }
            return ResponseService.json(201, res, 'Thesis deleted successfully');
        } catch (error) {
            return ResponseService.error(error, res);
        }
    }
};

